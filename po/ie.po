# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-31 00:52+0100\n"
"PO-Revision-Date: 2013-07-03 18:40+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Interlingue (http://www.transifex.com/xfce/xfce-apps/language/ie/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ie\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/screenshooter-dialogs.c:523
#, c-format
msgid "%.2fKb of %.2fKb"
msgstr "%.2fKb ex %.2fKb"

#: ../lib/screenshooter-dialogs.c:617
msgid "Transfer"
msgstr "Transferter"

#: ../lib/screenshooter-dialogs.c:638
msgid ""
"<span weight=\"bold\" stretch=\"semiexpanded\">The screenshot is being "
"transferred to:</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">Li captura esset transfertet a:</span>"

#. Setup window
#: ../lib/screenshooter-dialogs.c:879 ../lib/screenshooter-dialogs.c:888
#: ../lib/screenshooter-dialogs.c:1116 ../lib/screenshooter-utils.c:148
#: ../lib/screenshooter-utils.c:192 ../lib/screenshooter-imgur-dialog.c:128
#: ../src/xfce4-screenshooter.desktop.in.in.h:1
#: ../panel-plugin/screenshooter.desktop.in.h:1
msgid "Screenshot"
msgstr "Capter li ecran"

#: ../lib/screenshooter-dialogs.c:881 ../lib/screenshooter-dialogs.c:891
msgid "_Preferences"
msgstr "_Preferenties"

#: ../lib/screenshooter-dialogs.c:882 ../lib/screenshooter-dialogs.c:890
#: ../lib/screenshooter-dialogs.c:1118
msgid "_Help"
msgstr "Au_xilie"

#: ../lib/screenshooter-dialogs.c:883 ../lib/screenshooter-dialogs.c:1501
#: ../lib/screenshooter-imgur-dialog.c:130
msgid "_Close"
msgstr "_Cluder"

#: ../lib/screenshooter-dialogs.c:892 ../lib/screenshooter-dialogs.c:1120
#: ../lib/screenshooter-job-callbacks.c:172
msgid "_Cancel"
msgstr "_Anullar"

#: ../lib/screenshooter-dialogs.c:893 ../lib/screenshooter-dialogs.c:1121
#: ../lib/screenshooter-job-callbacks.c:173
msgid "_OK"
msgstr "_OK"

#: ../lib/screenshooter-dialogs.c:926
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Region to capture</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">Region por capter</span>"

#: ../lib/screenshooter-dialogs.c:948
msgid "Entire screen"
msgstr "Li tot ecran"

#: ../lib/screenshooter-dialogs.c:953 ../src/main.c:61
#: ../panel-plugin/screenshooter-plugin.c:311
msgid "Take a screenshot of the entire screen"
msgstr "Capter li tot ecran"

#: ../lib/screenshooter-dialogs.c:962
msgid "Active window"
msgstr "Activ fenestre"

#: ../lib/screenshooter-dialogs.c:967 ../src/main.c:108
#: ../panel-plugin/screenshooter-plugin.c:316
msgid "Take a screenshot of the active window"
msgstr "Capturar li actual fenestre"

#: ../lib/screenshooter-dialogs.c:976
msgid "Select a region"
msgstr "Selecte un region"

#: ../lib/screenshooter-dialogs.c:981
msgid ""
"Select a region to be captured by clicking a point of the screen without releasing the mouse button, dragging your mouse to the other corner of the region, and releasing the mouse button.\n"
"\n"
"Press Ctrl while dragging to move the region."
msgstr ""

#: ../lib/screenshooter-dialogs.c:994
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Options</span>"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1014
msgid "Capture the mouse pointer"
msgstr "Capter li apuntator de mus"

#: ../lib/screenshooter-dialogs.c:1018
msgid "Display the mouse pointer on the screenshot"
msgstr "Monstrar li apuntator in li captura"

#: ../lib/screenshooter-dialogs.c:1025
msgid "Capture the window border"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1030
msgid ""
"Display the window border on the screenshot.\n"
"Disabling this option has no effect for CSD windows."
msgstr ""

#: ../lib/screenshooter-dialogs.c:1047
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Delay before capturing</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">Retarde ante captura</span>"

#: ../lib/screenshooter-dialogs.c:1071
msgid "Delay in seconds before the screenshot is taken"
msgstr "Retarde in secondes ante que li ecran es capturat"

#: ../lib/screenshooter-dialogs.c:1074
msgid "seconds"
msgstr "secondes"

#: ../lib/screenshooter-dialogs.c:1119
msgid "_Back"
msgstr "_Retro"

#: ../lib/screenshooter-dialogs.c:1159
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Action</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">Action</span>"

#. Save option radio button
#: ../lib/screenshooter-dialogs.c:1174
msgid "Save"
msgstr "Gardar"

#: ../lib/screenshooter-dialogs.c:1180
msgid "Save the screenshot to a PNG file"
msgstr "Gardar li captura in un file PNG"

#. Show in folder checkbox
#: ../lib/screenshooter-dialogs.c:1184
msgid "Show in Folder"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1191
msgid "Shows the saved file in the folder"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1200
msgid "Copy to the clipboard"
msgstr "Copiar al Paperiere"

#: ../lib/screenshooter-dialogs.c:1202
msgid "Copy the screenshot to the clipboard so that it can be pasted later"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1216
msgid "Open with:"
msgstr "Aperter per:"

#: ../lib/screenshooter-dialogs.c:1224
msgid "Open the screenshot with the chosen application"
msgstr "Aperter li captura per li selectet application"

#: ../lib/screenshooter-dialogs.c:1243 ../src/main.c:76
msgid "Application to open the screenshot"
msgstr "Application per aperter li captura"

#: ../lib/screenshooter-dialogs.c:1257
msgid "Custom Action:"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1265
msgid "Execute the selected custom action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1277
msgid "Custom action to execute"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1302
msgid "Host on Imgur™"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1306
msgid "Host the screenshot on Imgur™, a free online image hosting service"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1318
msgid ""
"Watch for sensitive content, the uploaded image will be publicly\n"
"available and there is no guarantee it can be certainly deleted.\n"
"Xfce is NOT affiliated with nor this integration is approved by Imgur™.\n"
"If you use this feature you must agree with Imgur™ <a href=\"https://imgur.com/tos\">Terms of Service</a>."
msgstr ""

#: ../lib/screenshooter-dialogs.c:1349
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Preview</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">Previder</span>"

#: ../lib/screenshooter-dialogs.c:1410
msgid "Save screenshot as..."
msgstr "Gardar li captura quam..."

#: ../lib/screenshooter-dialogs.c:1428
msgid "PNG File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1429
msgid "JPG File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1430
msgid "BMP File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1433
msgid "WebP File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1499
msgid "Preferences"
msgstr "Preferenties"

#: ../lib/screenshooter-dialogs.c:1524
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Custom Actions</span>"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1543
msgid ""
"You can configure custom actions that will be available to handle "
"screenshots after they are captured."
msgstr ""

#: ../lib/screenshooter-dialogs.c:1573
msgid "Custom Action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1586
msgid "Add custom action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1593
msgid "Remove selected custom action"
msgstr ""

#. Add custom action name
#: ../lib/screenshooter-dialogs.c:1615
msgid "Name"
msgstr "Nómine"

#: ../lib/screenshooter-dialogs.c:1616
msgid "Name of the action that will be displayed in Actions dialog"
msgstr ""

#. Add custom action command
#: ../lib/screenshooter-dialogs.c:1625
msgid "Command"
msgstr "Comande"

#: ../lib/screenshooter-dialogs.c:1626
msgid "Command that will be executed for this custom action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1633
#, c-format
msgid "Use %f as a placeholder for location of the screenshot captured"
msgstr ""

#: ../lib/screenshooter-custom-actions.c:196
msgid "Unable to execute the custom action"
msgstr ""

#: ../lib/screenshooter-custom-actions.c:196
msgid "Invalid custom action selected"
msgstr ""

#: ../lib/screenshooter-custom-actions.c:208
#, c-format
msgid "Failed to run the custom action %s"
msgstr ""

#: ../lib/screenshooter-utils.c:315
#, c-format
msgid ""
"<b>The application could not be launched.</b>\n"
"%s"
msgstr "<b>Ne successat lansat li application.</b>\n%s"

#: ../lib/screenshooter-utils.c:390
msgid "Error"
msgstr "Errore"

#: ../lib/screenshooter-imgur.c:133
msgid "Upload the screenshot..."
msgstr "Cargar li captura..."

#: ../lib/screenshooter-imgur.c:166
#, c-format
msgid "An error occurred while transferring the data to imgur."
msgstr "Un errore evenit transfertente li data a imgur."

#: ../lib/screenshooter-imgur.c:231
msgid "Imgur"
msgstr "Imgur"

#: ../lib/screenshooter-imgur-dialog.ui.h:1
msgid "Size"
msgstr "Dimension"

#: ../lib/screenshooter-imgur-dialog.ui.h:2
msgid "Link"
msgstr "Ligament"

#: ../lib/screenshooter-imgur-dialog.ui.h:3
msgid "Tiny"
msgstr "Micrissim"

#: ../lib/screenshooter-imgur-dialog.ui.h:4
msgid "Medium"
msgstr "Medial"

#: ../lib/screenshooter-imgur-dialog.ui.h:5
msgid "Full"
msgstr "Complet"

#: ../lib/screenshooter-imgur-dialog.ui.h:6
msgid "Copy"
msgstr "Copiar"

#: ../lib/screenshooter-imgur-dialog.ui.h:7
msgid "View in browser"
msgstr "Vider in un navigator"

#: ../lib/screenshooter-imgur-dialog.ui.h:8
msgid "Image"
msgstr "Image"

#: ../lib/screenshooter-imgur-dialog.ui.h:9
msgid "Syntax"
msgstr "Sintaxe"

#: ../lib/screenshooter-imgur-dialog.ui.h:10
msgid "HTML"
msgstr "HTML"

#: ../lib/screenshooter-imgur-dialog.ui.h:11
msgid "Markdown"
msgstr "Markdown"

#: ../lib/screenshooter-imgur-dialog.ui.h:12
msgid "BBCODE"
msgstr "BBCODE"

#: ../lib/screenshooter-imgur-dialog.ui.h:13
msgid "Code"
msgstr "Code"

#: ../lib/screenshooter-imgur-dialog.ui.h:14
msgid "Type"
msgstr "Tip"

#: ../lib/screenshooter-imgur-dialog.ui.h:15
msgid "Direct image"
msgstr "Direct image"

#: ../lib/screenshooter-imgur-dialog.ui.h:16
msgid "Link to full size"
msgstr "Ligament al plen dimension"

#: ../lib/screenshooter-imgur-dialog.ui.h:17
msgid "Embed into code"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:18
msgid "Delete"
msgstr "Deleter"

#: ../lib/screenshooter-imgur-dialog.ui.h:19
msgid ""
"This link only shows up once. Make sure to save it if you think you might be"
" deleting this image. We don't currently support linking images to Imgur "
"accounts."
msgstr "Li ligament va aparir solmen un vez. Assecura que vu garda por remover ti-ci image in li futur. Li acuplation de images con contos Imgur ne es actualmen supportat."

#: ../lib/screenshooter-imgur-dialog.ui.h:20
msgid "Deletion link"
msgstr "Ligament por deletion"

#: ../lib/screenshooter-job-callbacks.c:73
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Status</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">Statu</span>"

#. Create the information dialog
#: ../lib/screenshooter-job-callbacks.c:170
msgid "Details about the screenshot"
msgstr "Detallies pri li captura"

#. Create the user label
#: ../lib/screenshooter-job-callbacks.c:213
msgid "User:"
msgstr "Usator:"

#: ../lib/screenshooter-job-callbacks.c:221
msgid ""
"Your user name, if you do not have one yet please create one on the Web page"
" linked above"
msgstr ""

#. Create the password label
#: ../lib/screenshooter-job-callbacks.c:227
msgid "Password:"
msgstr "Contrasigne:"

#: ../lib/screenshooter-job-callbacks.c:234
msgid "The password for the user above"
msgstr "Li contrasigne del usator"

#. Create the title label
#: ../lib/screenshooter-job-callbacks.c:240
msgid "Title:"
msgstr "Titul:"

#: ../lib/screenshooter-job-callbacks.c:248
msgid ""
"The title of the screenshot, it will be used when displaying the screenshot "
"on the image hosting service"
msgstr "Li titul de captura que va esser usat per li servicie de publication de images"

#. Create the comment label
#: ../lib/screenshooter-job-callbacks.c:254
msgid "Comment:"
msgstr "Comenta:"

#: ../lib/screenshooter-job-callbacks.c:262
msgid ""
"A comment on the screenshot, it will be used when displaying the screenshot "
"on the image hosting service"
msgstr "Un commente de captura que va esser usat per li servicie de publication de images"

#: ../src/main.c:51
msgid "Copy the screenshot to the clipboard"
msgstr "Copiar li captura al Paperiere"

#: ../src/main.c:56
msgid "Delay in seconds before taking the screenshot"
msgstr ""

#: ../src/main.c:66
msgid "Display the mouse on the screenshot"
msgstr "Monstrar li apuntator in li captura"

#: ../src/main.c:71
msgid "Removes the window border from the screenshot"
msgstr ""

#: ../src/main.c:81 ../panel-plugin/screenshooter-plugin.c:321
msgid ""
"Select a region to be captured by clicking a point of the screen without "
"releasing the mouse button, dragging your mouse to the other corner of the "
"region, and releasing the mouse button."
msgstr ""

#: ../src/main.c:88
msgid ""
"File path or directory where the screenshot will be saved, accepts png, jpg "
"and bmp extensions. webp is only supported if webp-pixbuf-loader is "
"installed."
msgstr ""

#: ../src/main.c:93
msgid "Show the saved file in the folder."
msgstr ""

#: ../src/main.c:98
msgid "Host the screenshot on Imgur, a free online image hosting service"
msgstr ""

#: ../src/main.c:103
msgid "Version information"
msgstr "Information del version"

#: ../src/main.c:131
#, c-format
msgid "Conflicting options: --%s and --%s cannot be used at the same time.\n"
msgstr ""

#: ../src/main.c:133
#, c-format
msgid ""
"The --%s option is only used when --fullscreen, --window or --region is "
"given. It will be ignored.\n"
msgstr ""

#: ../src/main.c:144
#, c-format
msgid ""
"%s: %s\n"
"Try %s --help to see a full list of available command line options.\n"
msgstr "%s: %s\nUsa %s --help por vider li complet liste de disponibil optiones.\n"

#: ../src/xfce4-screenshooter.desktop.in.in.h:2
#: ../panel-plugin/screenshooter.desktop.in.h:2
msgid ""
"Take screenshots of the entire screen, of the active window or of a region"
msgstr ""

#: ../src/xfce4-screenshooter.appdata.xml.in.h:1
msgid ""
"Allows you to capture the entire screen, the active window or a selected "
"region. You can set the delay that elapses before the screenshot is taken "
"and the action that will be done with the screenshot: save it to a PNG or "
"JPG file, copy it to the clipboard, open it using another application, or "
"host it on imgur, a free online image hosting service."
msgstr ""
