# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# محمد الحرقان <malham1@gmail.com>, 2012
# Oukiki Saleh <salehoukiki@gmail.com>, 2016
# محمد الحرقان <malham1@gmail.com>, 2012-2013
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-31 00:52+0100\n"
"PO-Revision-Date: 2013-07-03 18:40+0000\n"
"Last-Translator: Oukiki Saleh <salehoukiki@gmail.com>, 2016\n"
"Language-Team: Arabic (http://www.transifex.com/xfce/xfce-apps/language/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: ../lib/screenshooter-dialogs.c:523
#, c-format
msgid "%.2fKb of %.2fKb"
msgstr "%.2fKb من %.2fKb"

#: ../lib/screenshooter-dialogs.c:617
msgid "Transfer"
msgstr "نقل"

#: ../lib/screenshooter-dialogs.c:638
msgid ""
"<span weight=\"bold\" stretch=\"semiexpanded\">The screenshot is being "
"transferred to:</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">يجري نقل لقطة الشاشة:</span>"

#. Setup window
#: ../lib/screenshooter-dialogs.c:879 ../lib/screenshooter-dialogs.c:888
#: ../lib/screenshooter-dialogs.c:1116 ../lib/screenshooter-utils.c:148
#: ../lib/screenshooter-utils.c:192 ../lib/screenshooter-imgur-dialog.c:128
#: ../src/xfce4-screenshooter.desktop.in.in.h:1
#: ../panel-plugin/screenshooter.desktop.in.h:1
msgid "Screenshot"
msgstr "لقطة الشاشة"

#: ../lib/screenshooter-dialogs.c:881 ../lib/screenshooter-dialogs.c:891
msgid "_Preferences"
msgstr "_خيارات"

#: ../lib/screenshooter-dialogs.c:882 ../lib/screenshooter-dialogs.c:890
#: ../lib/screenshooter-dialogs.c:1118
msgid "_Help"
msgstr "_مساعدة"

#: ../lib/screenshooter-dialogs.c:883 ../lib/screenshooter-dialogs.c:1501
#: ../lib/screenshooter-imgur-dialog.c:130
msgid "_Close"
msgstr "_أغلق"

#: ../lib/screenshooter-dialogs.c:892 ../lib/screenshooter-dialogs.c:1120
#: ../lib/screenshooter-job-callbacks.c:172
msgid "_Cancel"
msgstr "_إلغاء"

#: ../lib/screenshooter-dialogs.c:893 ../lib/screenshooter-dialogs.c:1121
#: ../lib/screenshooter-job-callbacks.c:173
msgid "_OK"
msgstr ""

#: ../lib/screenshooter-dialogs.c:926
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Region to capture</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">منطقة لالتقاط</span>"

#: ../lib/screenshooter-dialogs.c:948
msgid "Entire screen"
msgstr "كامل الشاشة"

#: ../lib/screenshooter-dialogs.c:953 ../src/main.c:61
#: ../panel-plugin/screenshooter-plugin.c:311
msgid "Take a screenshot of the entire screen"
msgstr "تأخذ لقطة للشاشة بأكملها"

#: ../lib/screenshooter-dialogs.c:962
msgid "Active window"
msgstr "نافذة نشيطة"

#: ../lib/screenshooter-dialogs.c:967 ../src/main.c:108
#: ../panel-plugin/screenshooter-plugin.c:316
msgid "Take a screenshot of the active window"
msgstr "تأخذ لقطة للنافذة النشطة"

#: ../lib/screenshooter-dialogs.c:976
msgid "Select a region"
msgstr "تحديد منطقة"

#: ../lib/screenshooter-dialogs.c:981
msgid ""
"Select a region to be captured by clicking a point of the screen without releasing the mouse button, dragging your mouse to the other corner of the region, and releasing the mouse button.\n"
"\n"
"Press Ctrl while dragging to move the region."
msgstr ""

#: ../lib/screenshooter-dialogs.c:994
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Options</span>"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1014
msgid "Capture the mouse pointer"
msgstr "التقاط مؤشر الفارة"

#: ../lib/screenshooter-dialogs.c:1018
msgid "Display the mouse pointer on the screenshot"
msgstr "عرض مؤشر الفارة على شاشة"

#: ../lib/screenshooter-dialogs.c:1025
msgid "Capture the window border"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1030
msgid ""
"Display the window border on the screenshot.\n"
"Disabling this option has no effect for CSD windows."
msgstr ""

#: ../lib/screenshooter-dialogs.c:1047
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Delay before capturing</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">تأخير قبل التقاط الشاشة</span>"

#: ../lib/screenshooter-dialogs.c:1071
msgid "Delay in seconds before the screenshot is taken"
msgstr "تأخير يالثوان قبل التقاط لقطة الشاشة"

#: ../lib/screenshooter-dialogs.c:1074
msgid "seconds"
msgstr "ثانية"

#: ../lib/screenshooter-dialogs.c:1119
msgid "_Back"
msgstr "_للخلف"

#: ../lib/screenshooter-dialogs.c:1159
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Action</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">تأثير</span>"

#. Save option radio button
#: ../lib/screenshooter-dialogs.c:1174
msgid "Save"
msgstr "حفظ"

#: ../lib/screenshooter-dialogs.c:1180
msgid "Save the screenshot to a PNG file"
msgstr "حفظ لقطة الشاشة بصيغة PNG"

#. Show in folder checkbox
#: ../lib/screenshooter-dialogs.c:1184
msgid "Show in Folder"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1191
msgid "Shows the saved file in the folder"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1200
msgid "Copy to the clipboard"
msgstr "نسخ إلى الحافظة"

#: ../lib/screenshooter-dialogs.c:1202
msgid "Copy the screenshot to the clipboard so that it can be pasted later"
msgstr "نسخة لقطة الشاشة إلى الحافظة بحيث يمكن لصقه في وقت لاحق"

#: ../lib/screenshooter-dialogs.c:1216
msgid "Open with:"
msgstr "فتح باستخدام:"

#: ../lib/screenshooter-dialogs.c:1224
msgid "Open the screenshot with the chosen application"
msgstr "فتح لقطة الشاشة باستخدام التطبيق المختار"

#: ../lib/screenshooter-dialogs.c:1243 ../src/main.c:76
msgid "Application to open the screenshot"
msgstr "تطبيق لفتح القطة شاشة"

#: ../lib/screenshooter-dialogs.c:1257
msgid "Custom Action:"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1265
msgid "Execute the selected custom action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1277
msgid "Custom action to execute"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1302
msgid "Host on Imgur™"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1306
msgid "Host the screenshot on Imgur™, a free online image hosting service"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1318
msgid ""
"Watch for sensitive content, the uploaded image will be publicly\n"
"available and there is no guarantee it can be certainly deleted.\n"
"Xfce is NOT affiliated with nor this integration is approved by Imgur™.\n"
"If you use this feature you must agree with Imgur™ <a href=\"https://imgur.com/tos\">Terms of Service</a>."
msgstr ""

#: ../lib/screenshooter-dialogs.c:1349
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Preview</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">معاينة</span>"

#: ../lib/screenshooter-dialogs.c:1410
msgid "Save screenshot as..."
msgstr "حفظ لقطة الشاشة باسم..."

#: ../lib/screenshooter-dialogs.c:1428
msgid "PNG File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1429
msgid "JPG File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1430
msgid "BMP File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1433
msgid "WebP File"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1499
msgid "Preferences"
msgstr "تفضيلات"

#: ../lib/screenshooter-dialogs.c:1524
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Custom Actions</span>"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1543
msgid ""
"You can configure custom actions that will be available to handle "
"screenshots after they are captured."
msgstr ""

#: ../lib/screenshooter-dialogs.c:1573
msgid "Custom Action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1586
msgid "Add custom action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1593
msgid "Remove selected custom action"
msgstr ""

#. Add custom action name
#: ../lib/screenshooter-dialogs.c:1615
msgid "Name"
msgstr "اسم"

#: ../lib/screenshooter-dialogs.c:1616
msgid "Name of the action that will be displayed in Actions dialog"
msgstr ""

#. Add custom action command
#: ../lib/screenshooter-dialogs.c:1625
msgid "Command"
msgstr "أمر"

#: ../lib/screenshooter-dialogs.c:1626
msgid "Command that will be executed for this custom action"
msgstr ""

#: ../lib/screenshooter-dialogs.c:1633
#, c-format
msgid "Use %f as a placeholder for location of the screenshot captured"
msgstr ""

#: ../lib/screenshooter-custom-actions.c:196
msgid "Unable to execute the custom action"
msgstr ""

#: ../lib/screenshooter-custom-actions.c:196
msgid "Invalid custom action selected"
msgstr ""

#: ../lib/screenshooter-custom-actions.c:208
#, c-format
msgid "Failed to run the custom action %s"
msgstr ""

#: ../lib/screenshooter-utils.c:315
#, c-format
msgid ""
"<b>The application could not be launched.</b>\n"
"%s"
msgstr "<b>.لا يمكن تشغيل التطبيق</b>\n%s"

#: ../lib/screenshooter-utils.c:390
msgid "Error"
msgstr "خطأ"

#: ../lib/screenshooter-imgur.c:133
msgid "Upload the screenshot..."
msgstr "تحميل لقطة الشاشة..."

#: ../lib/screenshooter-imgur.c:166
#, c-format
msgid "An error occurred while transferring the data to imgur."
msgstr "حدث خطأ أثناء تحويل البيانات نحو imgur."

#: ../lib/screenshooter-imgur.c:231
msgid "Imgur"
msgstr "Imgur"

#: ../lib/screenshooter-imgur-dialog.ui.h:1
msgid "Size"
msgstr "الحجم"

#: ../lib/screenshooter-imgur-dialog.ui.h:2
msgid "Link"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:3
msgid "Tiny"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:4
msgid "Medium"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:5
msgid "Full"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:6
msgid "Copy"
msgstr "نسخة"

#: ../lib/screenshooter-imgur-dialog.ui.h:7
msgid "View in browser"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:8
msgid "Image"
msgstr "صورة"

#: ../lib/screenshooter-imgur-dialog.ui.h:9
msgid "Syntax"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:10
msgid "HTML"
msgstr "HTML"

#: ../lib/screenshooter-imgur-dialog.ui.h:11
msgid "Markdown"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:12
msgid "BBCODE"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:13
msgid "Code"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:14
msgid "Type"
msgstr "النوع"

#: ../lib/screenshooter-imgur-dialog.ui.h:15
msgid "Direct image"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:16
msgid "Link to full size"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:17
msgid "Embed into code"
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:18
msgid "Delete"
msgstr "حذف"

#: ../lib/screenshooter-imgur-dialog.ui.h:19
msgid ""
"This link only shows up once. Make sure to save it if you think you might be"
" deleting this image. We don't currently support linking images to Imgur "
"accounts."
msgstr ""

#: ../lib/screenshooter-imgur-dialog.ui.h:20
msgid "Deletion link"
msgstr ""

#: ../lib/screenshooter-job-callbacks.c:73
msgid "<span weight=\"bold\" stretch=\"semiexpanded\">Status</span>"
msgstr "<span weight=\"bold\" stretch=\"semiexpanded\">حالة</span>"

#. Create the information dialog
#: ../lib/screenshooter-job-callbacks.c:170
msgid "Details about the screenshot"
msgstr ""

#. Create the user label
#: ../lib/screenshooter-job-callbacks.c:213
msgid "User:"
msgstr "المستخدم:"

#: ../lib/screenshooter-job-callbacks.c:221
msgid ""
"Your user name, if you do not have one yet please create one on the Web page"
" linked above"
msgstr ""

#. Create the password label
#: ../lib/screenshooter-job-callbacks.c:227
msgid "Password:"
msgstr "كلمة المرور:"

#: ../lib/screenshooter-job-callbacks.c:234
msgid "The password for the user above"
msgstr "كلمة سر المستخدم"

#. Create the title label
#: ../lib/screenshooter-job-callbacks.c:240
msgid "Title:"
msgstr "العنوان:"

#: ../lib/screenshooter-job-callbacks.c:248
msgid ""
"The title of the screenshot, it will be used when displaying the screenshot "
"on the image hosting service"
msgstr ""

#. Create the comment label
#: ../lib/screenshooter-job-callbacks.c:254
msgid "Comment:"
msgstr "التعليق:"

#: ../lib/screenshooter-job-callbacks.c:262
msgid ""
"A comment on the screenshot, it will be used when displaying the screenshot "
"on the image hosting service"
msgstr ""

#: ../src/main.c:51
msgid "Copy the screenshot to the clipboard"
msgstr "انسخ اللقطة إلى الحافظة"

#: ../src/main.c:56
msgid "Delay in seconds before taking the screenshot"
msgstr "تأخير بالثواني قبل اتخاذ لقطة الشاشة"

#: ../src/main.c:66
msgid "Display the mouse on the screenshot"
msgstr "عرض الفارة على شاشة"

#: ../src/main.c:71
msgid "Removes the window border from the screenshot"
msgstr ""

#: ../src/main.c:81 ../panel-plugin/screenshooter-plugin.c:321
msgid ""
"Select a region to be captured by clicking a point of the screen without "
"releasing the mouse button, dragging your mouse to the other corner of the "
"region, and releasing the mouse button."
msgstr "اختر المنطقة التي سيتم التقاطها عن طريق النقر على نقطة في الشاشة من دون تحرير زر الفارة ، سحب الفأرة إلى الزاوية أخرى من المنطقة، ثم حرير زر الفاره."

#: ../src/main.c:88
msgid ""
"File path or directory where the screenshot will be saved, accepts png, jpg "
"and bmp extensions. webp is only supported if webp-pixbuf-loader is "
"installed."
msgstr ""

#: ../src/main.c:93
msgid "Show the saved file in the folder."
msgstr ""

#: ../src/main.c:98
msgid "Host the screenshot on Imgur, a free online image hosting service"
msgstr ""

#: ../src/main.c:103
msgid "Version information"
msgstr "معلومات الإصدار"

#: ../src/main.c:131
#, c-format
msgid "Conflicting options: --%s and --%s cannot be used at the same time.\n"
msgstr "خيارات الإعداد: --%s و --%s لا يمكن استخدامهما في نفس الوقت.\n"

#: ../src/main.c:133
#, c-format
msgid ""
"The --%s option is only used when --fullscreen, --window or --region is "
"given. It will be ignored.\n"
msgstr "ال --%s إعداد يستخدم فقط مع ---fullscreen, --window أو --region محددة. سيتم تجاهله.\n"

#: ../src/main.c:144
#, c-format
msgid ""
"%s: %s\n"
"Try %s --help to see a full list of available command line options.\n"
msgstr "%s: %s\nجرب %s --help لعرض الخيارات المتاحة لسطر الأوامر.\n"

#: ../src/xfce4-screenshooter.desktop.in.in.h:2
#: ../panel-plugin/screenshooter.desktop.in.h:2
msgid ""
"Take screenshots of the entire screen, of the active window or of a region"
msgstr "أخذ لقطات من الشاشة بأكملها ، من الإطار النشط أو منطقة"

#: ../src/xfce4-screenshooter.appdata.xml.in.h:1
msgid ""
"Allows you to capture the entire screen, the active window or a selected "
"region. You can set the delay that elapses before the screenshot is taken "
"and the action that will be done with the screenshot: save it to a PNG or "
"JPG file, copy it to the clipboard, open it using another application, or "
"host it on imgur, a free online image hosting service."
msgstr ""
